<?php

namespace LabelBuilder;

class Builder {

    /* Printer config */
    protected $dpmm = 8;
    protected $width_mm = 65;

    /* Label config */
    protected $origin = ["x" => 2, "y" => 2];
    protected $copies = 1;

    /* State */
    protected $font = "E";
    protected $position = ["x" => 0, "y" => 0];
    protected $spacing = 1;

    /* Commands */
    protected $commands = [];

    /* Config setters */

    public function setDpmm($dpmm) {
        $this->dpmm = $dpmm;
    }

    public function setWidthMm($mm) {
        $this->width_mm = $mm;
    }

    public function setOrigin($x = 0, $y = 0) {
        $this->origin = ["x" => $x, "y" => $y];
    }

    public function copies($quantity) {
        $this->copies = $quantity;
    }

    /* State setters */
    
    /**
     * https://files.tracerplus.com/public/docs/3rdparty/manuals/ZPL_Vol1.pdf#page=37
     */
    public function setFont($newFont) {
        $this->font = $newFont;
    }
    
    public function setPosition($x, $y){
        $this->position = ["x" => $x, "y" => $y];
    }
    
    public function setSpacing($spacing){
        $this->spacing = $spacing;
    }
    
    public function moveDown($lineHeight){
        $this->position["y"] += $lineHeight;
    }
    
    public function moveRight($padding){
        $this->position["x"] += $padding;
    }

    public function newLine(){
        $this->moveDown($this->getLineHeight());
    }

    /* Getters */

    /**
     * Get the width of the label in dots.
     * for 203dpi printers: 65mm * 8dpmm = 520 dots
     * for 300dpi printers: 65mm * 12dpmm = 780 dots
     */
    protected function getWidth() {
        return $this->dpmm * $this->width_mm;
    }
    
    protected function getLineHeight(){
        return [
            "A" => 2.25,
            "B" => 2.75,
            "C" => 3.125,
            "D" => 3.125,
            "E" => 5.625
        ][$this->font] * $this->spacing;
    }
    
    public function getOffset(){
        $x =  $this->position["x"] * $this->dpmm;
        $y =  $this->position["y"] * $this->dpmm;
        return "^FO$x,$y";
    }

    /* Commands */

    public function add($command){
        $this->commands[] = $command;
    }
    
    public function write($text){
        $pos = $this->getOffset();
        $font = "^A" . $this->font;
        $this->add("$pos$font^FD$text^FS");
    }

    public function addLine($text) {
        $this->write($text);
        $this->newLine();
    }

    public function box($w, $h = 0, $t = 1) {
        $pos = $this->getOffset();
        $w *= $this->dpmm;
        $h *= $this->dpmm;
        $this->add("$pos^GB$w,$h,$t^FS");
    }
    
    public function writeBlock($text, $w, $align = "L"){
        // mm to dots conversion
        $w *= $this->dpmm;
        // https://files.tracerplus.com/public/docs/3rdparty/manuals/ZPL_Vol1.pdf#page=175
        $a = $w;     // width of text block
        $b = 9999;   // maximum number of lines
        $c = 0;      // line spacing
        $d = $align; // text justification (L,R,C,J)
        $e = 0;      // indent
        $this->add("^FB$a,$b,$c,$d,$e");
        $this->write($text);
    }

    public function frame($w,$h){
        $w *= $this->dpmm;
        $h *= $this->dpmm;
        $this->add("^GB$w,$h,1");
    }

    /**
     * https://support.zebra.com/cpws/docs/zpl/zpl-zbi2-pm-en.pdf#page=80
     */
    public function barcode($text, $heigthMm = 12, $printLine = true, $width = 2){
        $pos = $this->getOffset();
        $o = "N"; // N,R,I,B
        $h = $heigthMm * $this->dpmm;
        $f = $printLine ? "Y" : "N";
        $font = "^BC$o,$h,$f,N,N,A";
        $this->add("$pos^BY$width$font^FD$text^FS");
    }

    public function field($name, $value, $font = "C", $trimAfter = 0, $marker = "...") {
        if ($trimAfter) {
            $value = mb_strimwidth($value, 0, $trimAfter, $marker);
        }
        $this->setFont("A");
        $this->write($name);
        $this->setFont($font);
        $this->moveDown(1.7);
        $this->write($value);
    }

    /* Tools */

    public function compile() {
        $x = $this->origin["x"] * $this->dpmm;
        $y = $this->origin["y"] * $this->dpmm;
        $width = $this->getWidth();
        return join([
            "^XA", // Start label
            "^PW$width", // Set print width
            "~SD25", // Darkness 25/30
            "^PR2", // Speed 4/14
            "^CI28", // Set encoding to UTF-8
            "^LH$x,$y", // Set Label Home
            "^PQ$this->copies", // Set Print Quantity
            ...$this->commands,
            "^XZ" // End label
        ]);
    }
    
    public function previewUrl($w, $h) {
        // mm to inches conversion
        $w *= 0.0393701;
        $h *= 0.0393701;
        $size = $w . "x" . $h;
        $zpl = $this->compile();
        $dpmm = $this->dpmm . "dpmm";	
        return "http://api.labelary.com/v1/printers/$dpmm/labels/$size/0/$zpl";
    }
    
    public function preview($w, $h) {
        $preview_url = $this->previewUrl($w, $h);
        return "<img src=\"$preview_url\" style='border:1px solid black; border-radius:8px' />";
    }

    public static function print(string $zpl, string $ip, float $timeout = 2) {
        $fp = fsockopen($ip, 9100, $error_code, $error_message, $timeout);
        if ($fp) {
            fwrite($fp, $zpl);
            fclose($fp);
        } else {
            throw new \Exception($error_message, $error_code);
        }
    }
}