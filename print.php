<?php

use LabelBuilder\Builder;
use LabelBuilder\Examples\FontsAndBoxes;

require __DIR__ . '/vendor/autoload.php';

if (count($argv) < 2) {
    die("Usage: php print.php <ip address>");
}

$ip = $argv[1];

$label = new FontsAndBoxes();

try {
    Builder::print($label->compile(), $ip);
} catch (\Exception $e) {
    echo "Error code: " . $e->getCode() . "\n";
    echo "Error message: " . $e->getMessage() . "\n";
}
