<?php

use LabelBuilder\Examples\ExchangeLabel;
use LabelBuilder\Examples\FontsAndBoxes;

require __DIR__ . '/vendor/autoload.php';

$dpmm = @$_GET["dpmm"] ?? 8;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Label Builder examples</title>
</head>
<body>
    <h1>Label Builder examples</h1>

    <form id="dpmmform">
        <select name="dpmm" onchange="dpmmform.submit()">
            <?php foreach ([6, 8, 12, 24] as $v): ?>
                <option
                    value="<?= $v ?>"
                    <?= $dpmm == $v ? "selected" : "" ?>
                ><?= $v ?> dpmm</option>
            <?php endforeach; ?>
        </select>
    </form>

    <h2>Fonts and boxes</h2>
    <p>Label dimensions are 65 x 18 mm. The default padding is 2mm. So the the inner box is 61 x 14 mm.</p>
    <p>Squares at the top are 1mm x 1mm.</p>
    <?= (new FontsAndBoxes($dpmm))->preview(65, 18) ?>
    
    <h2>Exchange label</h2>
    <?= (new ExchangeLabel($dpmm))->preview(65, 18) ?>
</body>
</html>