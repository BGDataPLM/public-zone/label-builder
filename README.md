# Label Builder

A PHP compiler to produce ZPL labels, for Zebra printers.

Use [Labelary](https://labelary.com/) API to produce a preview of the label.

Run examples:

```
php -S localhost:1234
```

Try printing:

```
php print.php 10.252.5.146
```