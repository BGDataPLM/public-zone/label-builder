<?php

namespace LabelBuilder\Examples;

use LabelBuilder\Builder;

class ExchangeLabel extends Builder
{

    public function __construct($dpmm = 8) {
        $this->setDpmm($dpmm);

        $this->setOrigin(0, 0);

        // ECHANGE vertical text
        $this->moveRight(1);
        $this->setFont("CB");
        $this->writeBlock("ECHANGE\&", 18, "C");

        // Vertical line
        $this->moveRight(2.5);
        $this->box(0, 18);
        $this->moveRight(1);

        // Username
        $this->setFont("BB");
        $this->writeBlock("perezj" . "\&", 18, "C");

        // Vertical line
        $this->moveRight(2);
        $this->box(0, 18);

        // Production order
        $this->setPosition(7.5, 1);
        $this->field("OF", "100119794");

        // Component
        $this->setPosition(7.5, 7);
        $this->field("Désignation composant", "TDM GG41 GMT ACI BLE WORLDTIMER", "C", 39);

        // Defect
        $this->setPosition(7.5, 13);
        $this->field("Qté", "2" . "x", "C");
        $this->setPosition(12, 13);
        $this->field("Type de défaut", "Défaut dimensionnel", "C", 36);

        $this->setPosition(35, 0);
        $this->barcode("100174619", 4);
    }

}
