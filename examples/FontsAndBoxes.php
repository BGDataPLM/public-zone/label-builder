<?php

namespace LabelBuilder\Examples;

use LabelBuilder\Builder;

class FontsAndBoxes extends Builder {

    public function __construct($dpmm = 8) {
        $this->setDpmm($dpmm);

        for ($i = 0; $i < 31; $i++) {
            $this->setPosition($i * 2, 0);
            $this->box(1, 1);
        }

        $this->setPosition(0, 2);
        $this->box(30, 12);

        foreach (['A', 'B', 'D', 'E'] as $font) {
            $this->setFont($font);
            $this->addLine("Font $font");
        }

        $this->setPosition(31, 2);
        $this->box(30, 12);

        foreach ([10, 15, 20, 25, 30] as $h) {
            $font = "0,$h";
            $this->setFont($font);
            $this->write("Scalable $h");
            $this->moveDown($h / $dpmm);
        }
    }
}